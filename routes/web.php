<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Frontend'], function(){
	
Route::get('/', 'ForntendController@index')->name('index');
Route::get('/jobs', 'ForntendController@jobs')->name('jobs');
Route::get('/job_details/{id}', 'ForntendController@job_details')->name('job_details');
Route::post('/job_applied/{id}', 'ForntendController@job_applied')->name('job_applied');
Route::post('/select_status_value/{id}', 'ForntendController@select_status_value')->name('select_status_value');
Route::post('/reject_status_value/{id}', 'ForntendController@reject_status_value')->name('reject_status_value');

Route::get('/contact', 'ForntendController@contact')->name('contact_us');


});
//For_Register_Page
Route::get('/create_account', 'UserController@create')->name('registration');
//For_Register_Form
Route::post('/create_account', 'UserController@registration_process');
//For_login_page
Route::get('/login', 'UserController@login')->name('login');
//For_login_Form
Route::post('/login', 'UserController@login_process');
Route::get('/logout', 'UserController@logout')->name('logout');
Route::get('/forget_password', 'UserController@forget_password')->name('forget_password');


Route::group(['namespace' => 'Frontend\Job_Seeker' , 'middleware' => 'auth'], function(){
	Route::get('/create_profile', 'JobSeekerController@create_profile')->name('create_profile');
	Route::post('/create_profile_process', 'JobSeekerController@create_profile_process')->name('create_profile_process');
	Route::get('/academic_qualification', 'JobSeekerController@academic_qua')->name('academic_qua');
	Route::post('/accademic_qualification_process', 'JobSeekerController@accademic_qualification_process')->name('accademic_qualification_process');
	Route::get('/view_resume', 'JobSeekerController@view_resume')->name('view_resume');
	Route::get('/job_seeker', 'JobSeekerController@job_seeker')->name('job_seeker');
	Route::get('/edit_resume', 'JobSeekerController@edit_resume')->name('edit_resume');
	Route::get('/training_summary', 'JobSeekerController@train_summary')->name('train_summary');
	Route::post('/training_summary_process', 'JobSeekerController@training_summary_process')->name('training_summary_process');
	Route::get('/seeker_applied_jobs', 'JobSeekerController@seeker_applied_jobs')->name('seeker_applied_jobs');
	Route::get('/notification_details/{id}', 'JobSeekerController@notification_details')->name('notification_details');
	Route::get('/emp_datils_by_application/{employer_id}', 'JobSeekerController@emp_datils_by_application')->name('emp_datils_by_application');

});




	Route::group(['namespace' => 'Frontend\Employers'], function(){
	Route::get('/employer', 'EmployerController@employer_dashbord')->name('employer_dashbord');
	Route::get('/create_employer_profile', 'EmployerController@employer_profile')->name('employer_profile');
	Route::post('/employer_profile_process', 'EmployerController@employer_profile_process')->name('employer_profile_process');
	Route::get('/view_employer_profile', 'EmployerController@employer_profile_view')->name('employer_profile_view');
	Route::get('/create_job', 'EmployerController@new_job')->name('new_job');
	Route::post('/create_job_process', 'EmployerController@create_job_process')->name('create_job_process');
	Route::get('/edit_job', 'EmployerController@edit_job')->name('edit_job');
	Route::post('/edit_job_details/{id}', 'EmployerController@edit_job_details')->name('edit_job_details');
	Route::get('/emp_job_list', 'EmployerController@emp_job_list')->name('emp_job_list');
	Route::get('/emp_job_details/{id}', 'EmployerController@emp_job_details')->name('emp_job_details');
	Route::get('/application_list/{id}', 'EmployerController@application_list')->name('application_list');
	Route::get('/emp_notification_details/{id}', 'EmployerController@emp_notification_details')->name('emp_notification_details');
	Route::get('/seeker_datils_by_job/{sekker_id}', 'EmployerController@seeker_datils_by_job')->name('seeker_datils_by_job');


});


	Route::group(['namespace' => 'Backend'], function(){

	Route::get('/admin', 'AdminController@admin_dashbord')->name('admin_dashbord');

	Route::get('/admin/user_list', 'AdminController@user_list')->name('user_list');

	Route::get('/admin/Job_list_admin', 'AdminController@Job_list_admin')->name('Job_list_admin');

	Route::get('/admin/job_seeker_list', 'AdminController@job_seeker_list')->name('job_seeker_list');

	Route::get('/admin/employers_list', 'AdminController@employers_list')->name('employers_list');

	Route::get('/admin/categories', 'AdminController@categories')->name('categories');

	Route::post('/admin/add_category_process', 'AdminController@add_category_process')->name('add_category_process');

	//Route::post('/add_location_process', 'AdminController@add_location_process')->name('add_location_process');

	Route::get('/admin/edit_category/{id}', 'AdminController@edit_category')->name('edit_category');

	Route::put('/edit_category/edit_category_process/{id}', 'AdminController@edit_category_process')->name('edit_category_process');

	Route::delete('/delete_category_process/{id}', 'AdminController@delete_category_process')->name('delete_category_process');

	Route::delete('/delete_location_process/{id}', 'AdminController@delete_location_process')->name('delete_location_process');

	Route::get('/admin/locations', 'AdminController@locations')->name('locations');
	

	Route::post('/add_location_process', 'AdminController@add_location_process')->name('add_location_process');

	Route::get('/admin/edit_location/{id}', 'AdminController@edit_location')->name('edit_location');

	Route::put('/edit_location/edit_location_process/{id}', 'AdminController@edit_location_process')->name('edit_location_process');

	Route::get('/admin/accademic_qualification_admin', 'AdminController@accademic_qualification_admin')->name('accademic_qualification_admin');

	Route::post('/add_qualification_process', 'AdminController@add_qualification_process')->name('add_qualification_process');

	Route::get('/admin/edit_qualification/{id}', 'AdminController@edit_qualification')->name('edit_qualification');

	Route::put('/edit_qualification_process/{id}', 'AdminController@edit_qualification_process')->name('edit_qualification_process');

	Route::delete('/delete_qualification_process/{id}', 'AdminController@delete_qualification_process')->name('delete_qualification_process');

	Route::get('/addmin_notification_details/{id}', 'AdminController@addmin_notification_details')->name('addmin_notification_details');

	Route::get('/edit_user_admin/{id}', 'AdminController@edit_user_admin')->name('edit_user_admin');

	Route::post('/admin_edit_user_process/{id}', 'AdminController@admin_edit_user_process')->name('admin_edit_user_process');

});






	Route::get('/user_dashbord', 'UserController@user_dashbord')->name('user_dashbord');



/*Route::auth();

Route::get('/home', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');*/

