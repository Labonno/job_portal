<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
             $table->string('job_id', 64)->nullable();
            $table->string('employer_id', 64)->nullable();
            $table->string('sekker_id', 64)->nullable();
            $table->string('job_title', 64)->nullable();
            $table->string('salary', 64)->nullable();
            $table->string('first_name', 64)->nullable();
            $table->string('last_name', 64)->nullable();
            $table->string('sec_deg', 64)->nullable();
            $table->string('exp_salary', 64)->nullable();
            $table->string('message', 64)->nullable();
            $table->tinyInteger('status')->default('0');
            $table->tinyInteger('admin_status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
