<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employer_id')->unsigned();
            $table->string('job_title', 32)->nullable();
            $table->longText('job_desc')->nullable();
            $table->string('job_cate', 32)->nullable();
            $table->integer('salary')->nullable();
            $table->string('dateline')->nullable();
            $table->integer('vacancies')->nullable();
            $table->string('location', 32)->nullable();
            $table->string('job_level', 32)->nullable();
            $table->string('candi_degree', 128)->nullable();
            $table->string('gender', 32)->nullable();
            $table->integer('age')->nullable();
            $table->integer('experience')->nullable();
            $table->longText('additional_req')->nullable();
            $table->longText('other_beni')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('new_jobs');
    }
}
