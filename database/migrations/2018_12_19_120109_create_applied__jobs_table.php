<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppliedJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applied__jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_id', 64)->nullable();
            $table->string('employer_id', 64)->nullable();
            $table->string('sekker_id', 64)->nullable();
            $table->string('seeker_first_name', 64)->nullable();
            $table->string('seeker_last_name', 64)->nullable();
            $table->string('seeker_deg', 64)->nullable();
            $table->string('seeker_gender', 64)->nullable();
            $table->string('job_title', 64)->nullable();
            $table->string('salary', 64)->nullable();
            $table->string('vacacy', 64)->nullable();
            $table->string('location', 64)->nullable();
            $table->string('cmp_name', 64)->nullable();
            $table->string('cmp_address', 64)->nullable();
            $table->string('exp_salary', 64)->nullable();
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applied__jobs');
    }
}
