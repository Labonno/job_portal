@extends('admin.master')

@section('contant')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="show_ammo">
                    <h3 class="ammo_head">Total Resume</h3>
                    <span class="ammo_icon"><i class="far fa-file"></i></span>
                    <p class="ammo_text">{{$user_count}}</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="show_ammo">
                    <h3 class="ammo_head">Total Jobs</h3>
                    <span class="ammo_icon"><i class="fas fa-network-wired"></i></span>
                    <p class="ammo_text">{{$jobs_count}}</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="show_ammo">
                    <h3 class="ammo_head">Todays Job</h3>
                    <span class="ammo_icon"><i class="fas fa-calendar-plus"></i></span>
                    <p class="ammo_text">{{$todaysJob}}</p>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection