<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="css/admin/assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Job Portal Dashbord</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('css/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('css/font-awesome.css" rel="stylesheet')}}" />

    <!-- Animation library for notifications   -->
    <link href="{{ asset('css/admin/assets/css/animate.min.css" rel="stylesheet')}}"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{ asset('css/admin/assets/css/light-bootstrap-dashboard.css?v=1.4.0')}}" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('css/admin/assets/css/demo.css')}}" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/admin/assets/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />

</head>
<body>

    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="css/admin/assets/img/sidebar-5.jpg">

            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="{{route('admin_dashbord')}}" class="simple-text">
                        Job Portal
                    </a>
                </div>

                <ul class="nav">
                    <li class="">
                        <a href="{{route('admin_dashbord')}}">
                            <i class="pe-7s-graph"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('user_list')}}">
                            <i class="pe-7s-user"></i>
                            <p>User List</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('categories')}}">
                            <i class="pe-7s-note2"></i>
                            <p>Manage Job Categories</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('locations')}}">
                            <i class="pe-7s-note2"></i>
                            <p>Manage Location</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('accademic_qualification_admin')}}">
                            <i class="pe-7s-note2"></i>
                            <p>Manage Academic Qualification</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Job_list_admin')}}">
                            <i class="pe-7s-user"></i>
                            <p>Job List</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('logout')}}">
                            <p>Log Out</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

<div class="main-panel">
    <nav class="navbar navbar-default navbar-fixed">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">Notification</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <p class="hidden-lg hidden-md"></p>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-globe"></i>
                                <b class="notification_count">{{$notification_number}}</b>
                                <p class="hidden-lg hidden-md">
                                    {{$notification_number}} Notifications
                                    <b class="caret"></b>
                                </p>
                        </a>
                        <ul class="dropdown-menu">
                            <p class="notify_drop">{{$notification_number}} Notifications</p>
                            @foreach($notification_data as $notification_datas)
                            <li><a href="{{route('addmin_notification_details', $notification_datas->id)}}">{{$notification_datas->job_title}} {{$notification_datas->message}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

@yield('contant')


<footer class="footer">
    <div class="container-fluid">
     <p>Job Portal System for LLC. Developed By <span>Sumon</span></p>
</div>
</footer>

</div>
</div>


</body>

<!--   Core JS Files   -->
<script src="{{ asset('css/admin/assets/js/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('css/admin/assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="{{ asset('css/admin/assets/js/chartist.min.js')}}"></script>

<!--  Notifications Plugin    -->
<script src="{{ asset('css/admin/assets/js/bootstrap-notify.js')}}"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="{{ asset('css/admin/assets/js/light-bootstrap-dashboard.js?v=1.4.0')}}"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="{{ asset('css/admin/assets/js/demo.js')}}"></script>

<script type="text/javascript">
   $(document).ready(function(){

       demo.initChartist();

       $.notify({
           message: "Welcome to <b>LLC Job Portal Dashboard</b>"

       },{
        type: 'info',
        timer: 4000
    });

   });

    // $('.delete-user').click(function(e){
    //     e.preventDefault() // Don't post the form, unless confirmed
    //     if (confirm('Are you sure?')) {
    //         // Post the form
    //         $(e.target).closest('form').submit() // Post the surrounding form
    //     }
    // });
</script>

</html>
