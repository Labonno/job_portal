@extends('master')


@section('contant')

<!--=============Job_Category Start Here=============-->
<section class="job_category bg_1 section_padding">
    <div class="row">
        <div class="col-md-9 all_cat job_details">
            @foreach($job_data as $job_datas)
            <div class="row ptb_20 bb_2">
                <h2 class="detail-title">Job Details</h2>
                <div class="col-md-6 col-sm-6 form-group">
                    <div class="input-group">
                        <span class="titlefix">
                            Job Title: 
                        </span>
                        <span class="title">
                            {{$job_datas->job_title}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="titlefix">
                            Job Categorie: 
                        </span>
                        <span class="title">
                            {{$job_datas->job_cate}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="titlefix">
                            Salary: 
                        </span>
                        <span class="title">
                            {{$job_datas->salary}}
                        </span>
                    </div>
                    <div class="input-group logo_fprm date_form">
                        <span class="titlefix">
                            DateLine: 
                        </span>
                        <span class="title">
                            {{$job_datas->dateline}}
                        </span>
                    </div>  
                </div>

                <div class="col-md-6 col-sm-6 form-group">
                    <div class="input-group">
                        <span class="titlefix">
                            No. of Vacancies: 
                        </span>
                        <span class="title">
                            {{$job_datas->vacancies}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="titlefix">
                            Location: 
                        </span>
                        <span class="title">
                            {{$job_datas->location}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="titlefix">
                            Job Level: 
                        </span>
                        <span class="title">
                            {{$job_datas->job_level}}
                        </span>
                    </div>
                </div>
                <div class="">
                    <span class="titlefix">
                        Job Description: 
                    </span>
                    <span class="title">
                        {{$job_datas->job_desc}}
                    </span>
                </div>
            </div>
            <div class="row ptb_20">
                <h2 class="detail-title">Candidates Requirements</h2>
                <div class="col-md-6 col-sm-6 form-group">
                    <div class="input-group">
                        <span class="titlefix">
                            Require Degree: 
                        </span>
                        <span class="title">
                            {{$job_datas->candi_degree}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="titlefix">
                            Age: 
                        </span>
                        <span class="title">
                            {{$job_datas->age}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="titlefix">
                            Additional Requirements: 
                        </span>
                        <span class="title">
                            {{$job_datas->additional_req}}
                        </span>
                    </div>  
                </div>

                <div class="col-md-6 col-sm-6 form-group">
                    <div class="input-group">
                        <span class="titlefix">
                            Experience: 
                        </span>
                        <span class="title">
                            {{$job_datas->experience}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="titlefix">
                            Gender: 
                        </span>
                        <span class="title">
                            {{$job_datas->gender}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="titlefix">
                            Compensation & Other Benefits: 
                        </span>
                        <span class="title">
                            {{$job_datas->other_beni}}
                        </span>
                    </div>

                    <div class="col-md-12 col-sm-12 mt_10">
                        @if((auth()->user()->user_type)=='job_seeker')
                        @if(!empty($check_apply))
                        <button class="btn btn-success btn-primary mid_btn align_right">Applied</button>
                        @else
                        <button class="btn btn-success btn-primary mid_btn align_right" data-toggle="modal" data-target="#ModalCenter">Apply</button>
                        <div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Are You Want To Go For Interview</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    @foreach($job_data as $job_datas)
                                    <div class="modal-body">

                                        <div class="modal_job_summ">
                                            <span class="mjs_con">
                                                {{$job_datas->job_title}}
                                            </span><br>
                                            <span class="mjs_con">
                                                Dateline: {{$job_datas->dateline}}
                                            </span><br>
                                            <span class="mjs_con">
                                                Vacancy: {{$job_datas->vacancies}}
                                            </span>
                                            <span class="mjs_con mjs_mar">
                                                Location: {{$job_datas->location}}
                                            </span>
                                        </div><br>
                                        <div class="modal_job_inp">
                                            @if (session()->has('message'))
                                            <div class="alert alert-success">
                                                {{ session('message') }}
                                            </div>
                                            @endif
                                            <form action="{{route('job_applied', $job_datas->id)}}" role="" method="post">
                                                {{csrf_field()}}
                                                <div class="input-group form-group">
                                                    <label class="exp_level" for="exp_salary">Expected Salary:</label><br>
                                                    <input type="text" id="exp_salary" name="exp_salary" class="form-control" placeholder="10000">
                                                    @if($errors->has('exp_salary'))
                                                    <span class="label label-danger">
                                                        {{ $errors->first('exp_salary') }}
                                                    </span>
                                                    @endif 
                                                </div>
                                                <div class="form-group">
                                                    <button type="Submit" class="btn btn-success">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary modal_btn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endif  
                    </div>      
                </div>
            </div>
            <div class="row ptb_20">    

            </div>
            @endforeach
        </div>
    </div>
</section>
<!--=============Job_Category End Here=============-->

@stop

@section('footer_widgets')

@include('partials.footer_widgets')

@endsection