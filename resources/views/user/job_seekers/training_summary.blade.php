@extends('user.master')

@section('contant')
<form action="{{route('training_summary_process')}}" role="" method="post" enctype="multipart/form-data">
	@csrf
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Traning Summary:</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<select class="form-control input-lg" name="title" placeholder="Traning Title">
					<option value="Web Development">Web Development</option>
					<option value="Android">Android</option>
					<option value="Wordpress">Wordpress</option>
					<option value="PHP">PHP</option>
					<option value="Laravel">Laravel</option>
					<option value="HTML">HTML</option>
					<option value="CSS">CSS</option>
					<option value="JAVA">JAVA</option>
					<option value="C#">C#</option>
					<option value="C">C</option>
					<option value="Paython">Paython</option>
					<option value="JAVA Script">JAVA Script</option>
				</select>
			</div>
			<div class="input-group">
				<input type="text" name="institute" class="form-control" placeholder="Institute" value="{{ old('institute') }}">
				@if($errors->has('institute'))
                    <span class="label label-danger">
                        {{ $errors->first('institute') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<select name="location" class="form-control input-lg">
					@foreach($location as $locations)
					<option>{{$locations->location_name}}</option>
					@endforeach
				</select>
				@if($errors->has('location'))
                    <span class="label label-danger">
                        {{ $errors->first('location') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<select class="form-control input-lg" name="duration" placeholder="Duration">
					<option value="Less than 1">Less than 1</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="More than 8">More than 8</option>
				</select>
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<input type="text" name="topic" class="form-control" placeholder="Topic" value="{{ old('topic') }}">
				@if($errors->has('topic'))
                    <span class="label label-danger">
                        {{ $errors->first('topic') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<input type="text" name="country" class="form-control" placeholder="Country" value="{{ old('country') }}">
				@if($errors->has('country'))
                    <span class="label label-danger">
                        {{ $errors->first('country') }}
                    </span>
                @endif
			</div>	
			<div class="input-group">
				<input type="text" name="year" class="form-control" placeholder="Year" value="{{ old('year') }}">
				@if($errors->has('year'))
                    <span class="label label-danger">
                        {{ $errors->first('year') }}
                    </span>
                @endif
			</div>				
		</div>
	</div>
	<div class="row ptb_20">	
		<div class="col-md-12 col-sm-12 mt_10">
			<button class="btn btn-success btn-primary mid_btn">Add</button>	
		</div>	
	</div>
</form>
@endsection