@extends('user.master')

@section('contant')
	<div class="row bottom-mrg">
        <div class="col-md-12 col-sm-12">
            <div class="advance-detail detail-desc-caption">
                <ul>
                    <li><strong class="j-applied">{{$notification_number}}</strong>Notifications</li>
                    <li><strong class="j-view">{{$job_cate}}</strong>New Post</li>
                    <li><strong class="j-applied">{{$job_seeker_job_count}}</strong>Job Applied</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
