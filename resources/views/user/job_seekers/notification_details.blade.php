@extends('user.master')

@section('contant')

<section class="job_category bg_1 section_padding">
    <div class="row">
        <div class="col-md-9 all_cat seeker_notification">
            <div class="row ptb_20 bb_2">
                <h2 class="detail-title">Message</h2>
                <div class="col-md-6 col-sm-6 form-group">
                    <div class="input-group">
                        <span class="title">
                            {{$notification->job_title}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="title">
                            {{$notification->salary}}
                        </span>
                    </div>
                    <div class="input-group logo_fprm date_form">
                    <span class="title">
                        {{$notification->message}}
                    </span>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>
@endsection