@extends('user.master')

@section('contant')
@if (session()->has('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
<form action="{{route('accademic_qualification_process')}}" role="" method="post" enctype="multipart/form-data">
	@csrf
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Academic Qualification:</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<select name="title" class="form-control input-lg">
					@foreach($qualification as $qualifications)
					<option>{{$qualifications->qualification_name}}</option>
					@endforeach
				</select>
				@if($errors->has('title'))
                    <span class="label label-danger">
                        {{ $errors->first('title') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<input type="text" name="major" class="form-control" placeholder="Major" value="{{ old('major') }}">
				@if($errors->has('major'))
                    <span class="label label-danger">
                        {{ $errors->first('major') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<input type="text" name="institute" class="form-control" placeholder="Institute" value="{{ old('institute') }}">
				@if($errors->has('institute'))
                    <span class="label label-danger">
                        {{ $errors->first('institute') }}
                    </span>
                @endif
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<input type="text" name="result" class="form-control" placeholder="Result" value="{{ old('result') }}">
				@if($errors->has('result'))
                    <span class="label label-danger">
                        {{ $errors->first('result') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<input type="text" name="scale" class="form-control" placeholder="Scale" value="{{ old('scale') }}">
				@if($errors->has('scale'))
                    <span class="label label-danger">
                        {{ $errors->first('scale') }}
                    </span>
                @endif
			</div>	
			<div class="input-group">
				<input type="text" name="year" class="form-control" placeholder="Pass Year" value="{{ old('year') }}">
				@if($errors->has('year'))
                    <span class="label label-danger">
                        {{ $errors->first('year') }}
                    </span>
                @endif
			</div>				
		</div>
	</div>
	<div class="row ptb_20">	
		<div class="col-md-12 col-sm-12 mt_10">
			<button class="btn btn-success btn-primary mid_btn">Add</button>	
		</div>	
	</div>
</form>
@endsection