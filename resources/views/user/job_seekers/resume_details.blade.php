@extends('user.master')

@section('contant')


<div class="row bottom-mrg col_text_center ptb_20">
	<div class="col-md-8 col-sm-8">
		<div class="detail-desc-caption">
			<h4>{{auth()->user()->first_name}} {{auth()->user()->last_name}}</h4>
			<span class="designation">{{ $data->sek_deg }}</span>
			
		</div>
		<div class="detail-desc-skill">
			@foreach($traning_data as $traning_skill)
			<span>{{$traning_skill->title}}</span>
			@endforeach
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
		<div class="get-touch">
			<h4>Get in Touch</h4>
			<ul>
				<li><i class="fa fa-map-marker"></i><span>{{$data->curent_location}}</span></li>
				<li><i class="fa fa-envelope"></i><span>{{auth()->user()->email}}</span></li>
				<li><i class="fa fa-phone"></i><span>{{auth()->user()->phone}}</span></li>
			</ul>
		</div>
	</div>
</div>
<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">About Resume</h2>
	<p>{{$data->sek_dec}}</p>
</div>

<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">Education</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	<table class="table table-striped table-dark">
		<thead>
			<tr>
				<th scope="col">Title</th>
				<th scope="col">major</th>
				<th scope="col">institute</th>
				<th scope="col">result</th>
				<th scope="col">scale</th>
				<th scope="col">year</th>
			</tr>
		</thead>
		<tbody>
			@foreach($education_data as $education_all_data)
			<tr>
				<td>{{$education_all_data->title}}</td>
				<td>{{$education_all_data->major}}</td>
				<td>{{$education_all_data->institute}}</td>
				<td>{{$education_all_data->result}}</td>
				<td>{{$education_all_data->scale}}</td>
				<td>{{$education_all_data->year}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">Personal Information</h2>
	<ul class="detail-list">
		<li><span class="per_info_title">Father Name: </span> <span>{{$data->father_name}}</span></li>
		<li><span class="per_info_title">Mother Name: </span> <span>{{$data->mother_name}}</span></li>
		<li><span class="per_info_title">Address: </span> <span>{{$data->address}}</span></li>
		<li><span class="per_info_title">Nationality: </span> <span>{{$data->nationality}}</span></li>
		<li><span class="per_info_title">Current Location: </span> <span>{{$data->curent_location}}</span></li>
		<li><span class="per_info_title">Date of Birth: </span> <span>{{$data->birth_date}}</span></li>
		<li><span class="per_info_title">Gender: </span> <span>{{$data->gender}}</span></li>
		<li><span class="per_info_title">Marige Status: </span> <span>{{$data->marital_status}}</span></li>
	</ul>
</div>
<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">Work Experience</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	<table class="table table-striped table-dark">
		<thead>
			<tr>
				<th scope="col">Title</th>
				<th scope="col">Institute</th>
				<th scope="col">Location</th>
				<th scope="col">Duration</th>
				<th scope="col">Topic</th>
				<th scope="col">Country</th>
				<th scope="col">Year</th>
			</tr>
		</thead>
		<tbody>
			@foreach($traning_data as $traning_all_data)
			<tr>
				<td>{{$traning_all_data->title}}</td>
				<td>{{$traning_all_data->institute}}</td>
				<td>{{$traning_all_data->location}}</td>
				<td>{{$traning_all_data->duration}}</td>
				<td>{{$traning_all_data->topic}}</td>
				<td>{{$traning_all_data->country}}</td>
				<td>{{$traning_all_data->year}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection