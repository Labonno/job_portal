<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Job Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

     
    <!--For Date Picker-->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <!--For Owl Carousel-->

    <!--For Owl Carousel-->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>
		
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body>
<!--===========Header Start Here===========-->
@include('partials.header')
<!--===========Header End Here===========-->
<!--===========User Header Start Here===========-->
@include('user.employers.partials.header')
<!--===========User Header End Here===========-->

<!--=============Page_Contant Start Here=============-->
<section class="user_dashbord_main">
    <div class="row">
        <div class="col-md-2">
            <aside class="job_seeker_main">
                <div class="sidebar left ">
                    <ul class="list-sidebar bg-defoult">
                        <li> 
                            <a href="{{route('employer_dashbord')}}">
                                <i class="fas fa-bars"></i>
                                <span class="nav-label">Dashboard</span>
                            </a> 
                        </li>
                        <li> 
                            <a href="{{route('employer_profile')}}">
                                <i class="fas fa-bars"></i>
                                <span class="nav-label">Create Profile</span>
                            </a> 
                        </li>
                        <li> 
                            <a href="{{route('employer_profile_view')}}">
                                <i class="fas fa-bars"></i>
                                <span class="nav-label">View Profile</span>
                            </a> 
                        </li>
                        <li> 
                            <a href="{{route('new_job')}}">
                                <i class="fas fa-bars"></i>
                                <span class="nav-label">Post a New Job</span>
                            </a> 
                        </li>
                        <li> 
                            <a href="{{route('emp_job_list')}}">
                                <i class="fas fa-bars"></i>
                                <span class="nav-label">Posted Jobs</span>
                            </a> 
                        </li>
                        <li>
                            <a href="{{route('logout')}}">
                                <i class="fas fa-bars"></i>
                                <span class="nav-label">Sign Out</span>
                            </a> 
                        </li>
                    </ul>
                </div>
            </aside>
        </div>
        <div class="col-md-10">
            @yield('contant')
        </div>
    </div>
</section>
<!--=============Page_Contant End Here=============-->
<!--=============Footer Start Here=============-->
@include('partials.footer')
<!--=============Footer End Here=============-->














    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
    <script src="{{ asset('js/plugins.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <!--For Owl Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!-- For Sticky Menu -->
    <script src="{{ asset('js/jquery.sticky.js') }}"></script>
    <!-- For Scroll Up -->
    <script src="{{ asset('js/jquery.scrollUp.js') }}"></script>
    <!-- Font Awesome -->
    <script src="{{ asset('js/all.js') }}"></script>
    <!--For Bootstrap Editor-->
    <script type="text/javascript" src="{{ asset('js/wysihtml5-0.3.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-wysihtml5.js') }}"></script>
    <!--For DatePicker-->
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <!--For Counter Up-->
    <script src="{{ asset('js/counterup.min.js') }}"></script>
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    

</body>
</html>

