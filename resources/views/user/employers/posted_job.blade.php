@extends('user.employers.master')

@section('contant')
<div class="row">
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-1.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-2.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-3.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-4.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-5.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-5.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-1.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-2.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-1.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6">
		<div class="manage-resume-box">
			<div class="col-md-3 col-sm-3">
				<div class="manage-resume-picbox">
					<img src="assets/img/client-2.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<h4>Senior Front-end Developer</h4>
				<span>Update: 10 Hour Ago</span>
			</div>
			<div class="col-md-1 col-sm-1">
				<a href="#" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
</div>
@endsection