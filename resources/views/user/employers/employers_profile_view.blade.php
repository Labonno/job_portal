@extends('user.employers.master')

@section('contant')
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Empolyers Details</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<label for="">Company Name: </label>
				<span class="em_profile_comon company_mame">
					{{$emp_data->cmp_name}}
				</span>
			</div>
			<div class="input-group">
				<label for="">City: </label>
				<span class="em_profile_comon company_city">
					{{$emp_data->cmp_city}}
				</span>
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<label for="">Tagline: </label>
				<span class="em_profile_comon company_tagline">
					{{$emp_data->cmp_tagline}}
				</span>
			</div>
			<div class="input-group">
				<label for="">Designation: </label>
				<span class="em_profile_comon user_designation">
					{{$emp_data->cnt_designation}}
				</span>
			</div>	
		</div>
	</div>
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Information</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<label for="">Email: </label>
				<span class="em_profile_comon">{{auth()->user()->email}}</span>
			</div>
			<div class="input-group">
				<label>Phone: </label>
				<span class="em_profile_comon">{{auth()->user()->phone}}</span>
			</div>
			<div class="input-group">
				<label for="designation">Website Address: </label>
				<span class="em_profile_comon web_address">
					{{$emp_data->cmp_website}}
				</span>
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<label for="">Address</label>
				<span class="em_profile_comon address">
					{{$emp_data->cmp_address}}
				</span>
			</div>
			<div class="input-group">
				<label for="designation">Employe Range</label>
				<span class="em_profile_comon emp_range">
					{{$emp_data->employe_rang}}
				</span>
			</div>	
		</div>
	</div>
	<div class="row ptb_20">
		<h2 class="detail-title">About Company</h2>
		
		<div class="col-md-12 col-sm-12">
			<span class="em_profile_comon em_about">
				{{$emp_data->cmp_dec}}
			</span>
		</div>
	</div>
@endsection