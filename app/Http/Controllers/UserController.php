<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Models\seeker_profile;
use App\Models\employer_profile;
use App\Http\Requests;
// use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function create(){
    	return view('user.registration');
    }

        public function login(){
        return view('user.login');
    }

    public function user_dashbord()
    {
        // dd(auth()->user()->user_type);
        return view('user.user_dashbord');
    }

    public function registration_process(Request $request){
    	
    	//Get All Input Data

    	$inputs = $request->except('_token');

     	$validator = Validator::make($inputs, [

			 'first_name' => 'required',
			 'last_name' => 'required',
			 'password' => 'required|min:2',
			 'email' => 'required|email',
			 'phone' => 'required',		
             'user_name' => 'required', 
			 'user_type' => 'required',	
     	]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
         }


    	// create user

        $user=User::create([
            'first_name'=>trim($request->input('first_name')),
            'last_name' => trim($request->input('last_name')),
            'password' => bcrypt($request->input('password')),
            'email' => trim($request->input('email')),
            'phone' => trim($request->input('phone')),
            'user_name' => trim($request->input('user_name')),
            'user_type' => trim($request->input('user_type')),

        ]);

        if(($user->user_type)=='job_seeker')
        {
            seeker_profile::create([
                'user_id'=>$user->id ,
            ]);
        }
        elseif(($user->user_type)=='employer')
        {
            employer_profile::create([
                'user_id'=>$user->id ,
            ]);
        }

        session()->flash('message', 'User registered successfully.');
        return redirect('/login');
    }

    public function login_process(Request $request)
    {

        $inputs = $request->only('email','password');

        $validator = Validator::make($inputs, [

             'password' => 'required|min:2',
             'email' => 'required|email',
        ]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
         }

        if (auth()->attempt($inputs)) {
            session()->flash('message', 'You have been logged in.');
            return redirect('/'.auth()->user()->user_type);
        }
        session()->flash('message', 'Please check your Email or Password');
        return redirect()->back();
    }

    public function logout()
    {
        auth()->logout();
        session()->flash('message', 'You have been logged out');
        return redirect()->route('index');
    }




}
