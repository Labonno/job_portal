<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Applied_Jobs extends Model
{
    protected $fillable = [
        'job_id','employer_id','sekker_id','seeker_first_name','seeker_last_name','seeker_deg','seeker_gender','job_title','salary','vacacy','location','cmp_name','cmp_address','exp_salary','status',
    ];
}
