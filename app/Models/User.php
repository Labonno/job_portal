<?php

namespace App\Models;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{

	protected $fillable = [
	'first_name','last_name', 'password', 'email', 'phone', 'user_name', 'user_type',
	];

}
