<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class seeker_profile extends Model
{

   protected $fillable = [
        'user_id', 'father_name','mother_name', 'curent_location', 'address', 'nationality', 'birth_date', 'sek_categoory', 'sek_deg', 'marital_status', 'gender', 'sek_dec', 'profile_photo',
    ];

    public function seeker_academic()
    {
    	return $this->hasOne('App\Models\academic_qualification', 'seeker_id','id');
    }
}
