<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class employer_profile extends Model
{
    protected $fillable = [
        'user_id', 'cmp_name','cmp_tagline', 'cmp_categoory', 'cmp_city', 'cnt_designation', 'cmp_website', 'cmp_address', 'employe_rang', 'cmp_logo', 'cmp_dec', ];
}
